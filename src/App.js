import React, { Component } from 'react';

function mod(n, m) {
  return ((n % m) + m) % m;
}

function sTableByNum(tableNum) {
  let tableToUse = [];
  if (tableNum === 1) {
    tableToUse = s1;
  } else if (tableNum === 2) {
    tableToUse = s2;
  } else if (tableNum === 3) {
    tableToUse = s3;
  } else if (tableNum === 4) {
    tableToUse = s4;
  } else if (tableNum === 5) {
    tableToUse = s5;
  } else if (tableNum === 6) {
    tableToUse = s6;
  } else if (tableNum === 7) {
    tableToUse = s7;
  } else if (tableNum === 8) {
    tableToUse = s8;
  }
  return tableToUse;
}

function sTableLookup(tableNum, values) {
  let row = parseInt(values.charAt(0) + values.charAt(5), 2);
  let col = parseInt(values.slice(1, 5), 2);
  let tableToUse = sTableByNum(tableNum);

  let toReturn = (tableToUse[row][col]).toString(2);
  // pad with 0s
  toReturn = "0".repeat(4 - toReturn.length) + toReturn;
  return toReturn;
}

class STable extends Component {

  test = "this is a test";

  getColor = (row, col, values) => {

    let targetRow = parseInt(values.charAt(0) + values.charAt(5), 2);
    let targetCol = parseInt(values.slice(1, 5), 2);
    console.log("this.row is " + this.row);
    console.log("this.col is " + this.col);
    console.log("test is " + this.test);
    if (row === targetRow && col < targetCol) {
      return "green";
    } else if (col === targetCol && row < targetRow) {
      return "blue";
    } else if (col === targetCol && row === targetRow) {
      return "orange";
    } else {
      return "";
    }
  }

  render() {
    if (this.props.values.length != 48) {
      return null;
    } else if (this.props.tableNum === '') {
      return (<p>Click on a table row</p>);
    }
    let values = this.props.values.slice(6 * (this.props.tableNum - 1), 6 * this.props.tableNum);
    let table = sTableByNum(this.props.tableNum);
    return (      
    <div>
      <div className="s-table-explainer">
        <h5>Using S-box #{this.props.tableNum} for row #{this.props.tableNum}</h5>
        <span class="green">{values.charAt(0)}</span>
        <span class="blue">{values.slice(1, 5)}</span>
        <span class="green">{values.charAt(5)}</span>
        <br />
        <br />
        <span class="green explainer-row">Row: {values.charAt(0) + values.charAt(5)}</span>
        <span class="blue explainer-row">Column: {values.slice(1, 5)}</span> 
        <span class="orange explainer-row">
          Substitution = {parseInt(sTableLookup(this.props.tableNum, values), 2)}(base 10) = {sTableLookup(this.props.tableNum, values)} (base 2)
        </span>
        <br />

      </div>
      <div class="table-responsive">
        
        <table className="table table-striped table-hover">
          <tbody>
            <tr>
              <td></td>
          {Array.from(Array(16).keys()).map((i) => {
            return <td class={"bold " + this.getColor(-1, i, values)}>{("0".repeat(4 - i.toString(2).length)) + i.toString(2)}</td>
          })}
          </tr>
          {table.map((row, index) =>{
              return(
            <tr key={index} id={`row${index}`}>
              <td class={"bold " + this.getColor(index, -1, values)}>{(index < 2 ? "0" : "") + index.toString(2)}</td>
              {row.map((item,index2)=>{
                return(
                  <td id={`cell${index}-{index2}`} className={this.getColor(index, index2, values)}>{item}</td>
                );  
                })}
            </tr>
            );
            })}
          </tbody>
        </table>
      </div>
    </div>

    );
  }
}

class ExpanderTable extends Component {
  render() {
    if (this.props.input.length !== this.props.rows * this.props.cols) {
      return (
        <p><br />Please enter exactly {this.props.numBits} bits of input as 1s and 0s</p>
      );
    }
    return (
    <div>
      <h4>{this.props.label}</h4>
      <table className="table table-striped table-hover">
        <tbody>
        {Array.from(Array(this.props.rows).keys()).map((item) =>{
            return(
          <tr key={item} onClick={this.props.onClicks != null ? () => this.props.onClicks(item + 1) : () => {}} id={`row${item}`}>
            {this.props.input.split("").slice(item * this.props.cols, (item * this.props.cols) + this.props.cols).map((item2,index2)=>{
              return(
                <td id={`cell${item}-{index2}`} className={"bold " + (this.props.classes == null ? "" : this.props.classes[item * this.props.cols + index2])}>{item2}</td>
              );  
              })}
          </tr>
          );
          })}
        </tbody>
      </table>
    </div>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanderInput: '',
      sBoxInput: '',
      pBoxInput: '',
      clickedTableNum: '',
    }
  }

  updateInput = (e) => {
    // make sure input updates
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  pPermute = (values) => {
    if (this.state.pBoxInput.length !== 32) {
      return "";
    }
    let result = "";
    p.forEach((row) => {
      row.forEach((item) => {
        result += values.charAt(parseInt(item, 10) -1);
      });
    });
    return result;
  }

  getSBoxed =() => {
    if (this.state.sBoxInput.length !== 48) {
      return "";
    }
    return (
      sTableLookup(1, this.state.sBoxInput.slice(0, 6)) + 
      sTableLookup(2, this.state.sBoxInput.slice(6, 12)) + 
      sTableLookup(3, this.state.sBoxInput.slice(12, 18)) + 
      sTableLookup(4, this.state.sBoxInput.slice(18, 24)) + 
      sTableLookup(5, this.state.sBoxInput.slice(24, 30)) + 
      sTableLookup(6, this.state.sBoxInput.slice(30, 36)) + 
      sTableLookup(7, this.state.sBoxInput.slice(36, 42)) + 
      sTableLookup(8, this.state.sBoxInput.slice(42, 48))
    );
  }

  getPreExpandedClasses = () => {
    let result = [];
    for (let i = 0; i < 16; i += 2) {
      result.push("n" + i);
      result.push("");
      result.push("");
      result.push("n" + (i + 1));
    }
    return result;
  }

  getExpandedClasses = () => {
    let result = [];
    for (let i = 0; i < 16; i += 2) {
      result.push("n" + mod(i - 1, 16) + " black-bg");
      result.push("n" + i);
      result.push("");
      result.push("");
      result.push("n" + (i + 1));
      result.push("n" + mod(i + 2, 16) + " black-bg");

    }
    return result;
  }

  getExpanded = () => {
    if (this.state.expanderInput.length !== 32) {
      return "";
    }
    return (
    this.state.expanderInput.charAt(31) + 
    this.state.expanderInput.slice(0, 4) + 
    this.state.expanderInput.charAt(4) + 
    this.state.expanderInput.charAt(3) + 
    this.state.expanderInput.slice(4, 8) + 
    this.state.expanderInput.charAt(8) + 
    this.state.expanderInput.charAt(7) + 
    this.state.expanderInput.slice(8, 12) + 
    this.state.expanderInput.charAt(12) + 
    this.state.expanderInput.charAt(11) + 
    this.state.expanderInput.slice(12, 16) + 
    this.state.expanderInput.charAt(16) + 
    this.state.expanderInput.charAt(15) + 
    this.state.expanderInput.slice(16, 20) + 
    this.state.expanderInput.charAt(20) + 
    this.state.expanderInput.charAt(19) + 
    this.state.expanderInput.slice(20, 24) + 
    this.state.expanderInput.charAt(24) + 
    this.state.expanderInput.charAt(23) + 
    this.state.expanderInput.slice(24, 28) + 
    this.state.expanderInput.charAt(28) + 
    this.state.expanderInput.charAt(27) + 
    this.state.expanderInput.slice(28, 32) + 
    this.state.expanderInput.charAt(0))
  }

  render() {
    return (
      <div className="container">
        <h1>DES Tools</h1>
        <br />
        <h3>DES Expander</h3>
        <br />
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="expanderInput">Input</label>
            <textarea className="form-control" 
                      name="expanderInput"
                      onChange={(e) => this.updateInput(e)}
                      value={this.state.expanderInput}>
            </textarea>
          </div>
          <div className="col-md-6 center-table">
            <ExpanderTable
              input={this.state.expanderInput} 
              cols={4}
              rows={8}
              label="Input"
              numBits={32}
              classes={this.getPreExpandedClasses()}
              />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="expanderOutput">Output</label>
            <textarea className="form-control" 
                      name="expanderOutput"
                      readOnly="readOnly"
                      value={this.getExpanded()}
                      >
            </textarea>
          </div>
          <div className="col-md-6">
            <ExpanderTable
              input={this.getExpanded()}
              cols={6}
              rows={8}
              numBits={32}
              classes={this.getExpandedClasses()}
              label="Output"
              />
          </div>
        </div>
        <br />
        <br />
        <h3>DES S-box Converter</h3>
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="sBoxInput">Input</label>
            <textarea className="form-control" 
                      name="sBoxInput"
                      onChange={(e) => this.updateInput(e)}
                      value={this.state.sBoxInput}>
            </textarea>
          </div>
          <div class="col-md-6">
            <label htmlFor="sBoxOutput">Output</label>
              <textarea className="form-control" 
                        name="sBoxOutput"
                        readOnly="readOnly"
                        value={this.getSBoxed()}
                        >
              </textarea>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <ExpanderTable
              input={this.state.sBoxInput} 
              cols={6}
              rows={8}
              label="Input"
              numBits={48}
              onClicks={(i) => this.setState({
                clickedTableNum: i,
              })}
              />
          </div>
          <div className="col-md-6">
            {/* <ExpanderTable
              input={this.getSBoxed()}
              cols={4}
              rows={8}
              numBits={48}
              label="Output"
              /> */}
              <STable
                values={this.state.sBoxInput}
                tableNum={this.state.clickedTableNum}
              />
          </div>
        </div>


        <br /><br />
        <h3>DES P-box Permutation</h3>
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="pBoxInput">Input</label>
            <textarea className="form-control" 
                      name="pBoxInput"
                      onChange={(e) => this.updateInput(e)}
                      value={this.state.pBoxInput}>
            </textarea>
          </div>
          <div className="col-md-6">
            <ExpanderTable
              input={this.state.pBoxInput} 
              cols={4}
              rows={8}
              label="Input"
              numBits={32}
              />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="pBoxOutput">Output</label>
            <textarea className="form-control" 
                      name="pBoxOutput"
                      readOnly="readOnly"
                      value={this.pPermute(this.state.pBoxInput)}
                      >
            </textarea>
          </div>
          <div className="col-md-6">
            <ExpanderTable
              input={this.pPermute(this.state.pBoxInput)}
              cols={4}
              rows={8}
              numBits={32}
              label="Output"
              />
          </div>
        </div>
        <br />
        <br />
        <h3>XOR Helper</h3>
        <Xor />
      </div>
    );
  }
}


class Xor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input1: '',
      input2: '',
      output: '',
      copy: 'Copy',
      error: false,
    };
  }

  copy = () => {
    var copyText = document.getElementById("output");
    copyText.select();
    document.execCommand('copy');
    this.setState({
      copy: 'Copied!',
    })
  }

  errorExists = () => {
    let badInput = false;
    (this.state.input1 + this.state.input2).split("").forEach(x => {
      if (x !== "0" && x !== "1") {
        this.setState({
          output: "Error: input can only be 1s and 0s",
          error: true,
        });
        badInput = true;
      }
    });

    if (badInput) {
      return true;
    }

    if (this.state.input1.length > this.state.input2.length) {
      this.setState({
        output: "Error: input 1 is longer than input 2 (they must be the same length)",
        error: true,
      });
      return true;
    } else if (this.state.input1.length < this.state.input2.length) {
      this.setState({
        output: "Error: input 1 is shorter than input 2 (they must be the same length)",
        error: true,
      });
      return true;
    }
    this.setState({
      error: false,
    });
    return false;
  }

  xor = (a, b) => {
    if (a === b) {
      return "0";
    } else {
      return "1";
    }
  }

  updateOutput = () => {
    if (this.errorExists()) {
      return;
    }

    let outputString = "";
    for (let i = 0; i < this.state.input1.length; i++) {
      outputString += this.xor(this.state.input1.charAt(i), this.state.input2.charAt(i));
    }

    this.setState({
      output: outputString,
    });
  }

  updateInput = (e) => {
    // make sure input updates
    this.setState({
      [e.target.name]: e.target.value,
      output: '',
      copy: 'Copy',
    }, () => this.updateOutput());
  }

  render() {
    return (
      <div>
      <label htmlFor="input1">
      Input 1:
      </label>
      <textarea 
        className="form-control"
        placeholder="Enter as 1s and 0s, no spaces"
        id="input1"
        name="input1"
        value={this.state.input1}
        onChange={(e) => this.updateInput(e)}
      />
      <br />
      <label htmlFor="input2">
      Input 2:
      </label>
      <textarea 
        className="form-control"
        placeholder="Enter as 1s and 0s, no spaces"
        id="input2"
        name="input2"
        value={this.state.input2}
        onChange={(e) => this.updateInput(e)}
      />
      <br />
      <label htmlFor="output">
      Output:
      </label>
      <textarea 
        className={"form-control" + (this.state.error ? " error-background" : "")}
        id="output"
        name="output"
        readOnly="readOnly"
        value={this.state.output}
      />
      <br />
      <button
        className="btn btn-outline-primary center-button"
        id="copy"
        onClick={() => this.copy()}
      >
        {this.state.copy}
      </button>
      <br /><br /><br />
      </div>
    );
  }
}

let s1 = [[14 , 4 , 13 , 1 , 2 , 15 , 11 , 8 , 3 , 10 , 6 , 12 , 5 , 9 , 0 , 7],
[0 , 15 , 7 , 4 , 14 , 2 , 13 , 1 , 10 , 6 , 12 , 11 , 9 , 5 , 3 , 8],
[4 , 1 , 14 , 8 , 13 , 6 , 2 , 11 , 15 , 12 , 9 , 7 , 3 , 10 , 5 , 0],
[15 , 12 , 8 , 2 , 4 , 9 , 1 , 7 , 5 , 11 , 3 , 14 , 10 , 0 , 6 , 13]];

let s2 = [[15 , 1 , 8 , 14 , 6 , 11 , 3 , 4 , 9 , 7 , 2 , 13 , 12 , 0 , 5 , 10],
[3 , 13 , 4 , 7 , 15 , 2 , 8 , 14 , 12 , 0 , 1 , 10 , 6 , 9 , 11 , 5],
[0 , 14 , 7 , 11 , 10 , 4 , 13 , 1 , 5 , 8 , 12 , 6 , 9 , 3 , 2 , 15],
[13 , 8 , 10 , 1 , 3 , 15 , 4 , 2 , 11 , 6 , 7 , 12 , 0 , 5 , 14 , 9]];

let s3 = [[10 , 0 , 9 , 14 , 6 , 3 , 15 , 5 , 1 , 13 , 12 , 7 , 11 , 4 , 2 , 8],
[13 , 7 , 0 , 9 , 3 , 4 , 6 , 10 , 2 , 8 , 5 , 14 , 12 , 11 , 15 , 1],
[13 , 6 , 4 , 9 , 8 , 15 , 3 , 0 , 11 , 1 , 2 , 12 , 5 , 10 , 14 , 7],
[1 , 10 , 13 , 0 , 6 , 9 , 8 , 7 , 4 , 15 , 14 , 3 , 11 , 5 , 2 , 12]];

let s4 = [[7 , 13 , 14 , 3 , 0 , 6 , 9 , 10 , 1 , 2 , 8 , 5 , 11 , 12 , 4 , 15],
[13 , 8 , 11 , 5 , 6 , 15 , 0 , 3 , 4 , 7 , 2 , 12 , 1 , 10 , 14 , 9],
[10 , 6 , 9 , 0 , 12 , 11 , 7 , 13 , 15 , 1 , 3 , 14 , 5 , 2 , 8 , 4],
[3 , 15 , 0 , 6 , 10 , 1 , 13 , 8 , 9 , 4 , 5 , 11 , 12 , 7 , 2 , 14]];

let s5 = [[2 , 12 , 4 , 1 , 7 , 10 , 11 , 6 , 8 , 5 , 3 , 15 , 13 , 0 , 14 , 9],
[14 , 11 , 2 , 12 , 4 , 7 , 13 , 1 , 5 , 0 , 15 , 10 , 3 , 9 , 8 , 6],
[4 , 2 , 1 , 11 , 10 , 13 , 7 , 8 , 15 , 9 , 12 , 5 , 6 , 3 , 0 , 14],
[11 , 8 , 12 , 7 , 1 , 14 , 2 , 13 , 6 , 15 , 0 , 9 , 10 , 4 , 5 , 3]];

let s6 = [[12 , 1 , 10 , 15 , 9 , 2 , 6 , 8 , 0 , 13 , 3 , 4 , 14 , 7 , 5 , 11],
[10 , 15 , 4 , 2 , 7 , 12 , 9 , 5 , 6 , 1 , 13 , 14 , 0 , 11 , 3 , 8],
[9 , 14 , 15 , 5 , 2 , 8 , 12 , 3 , 7 , 0 , 4 , 10 , 1 , 13 , 11 , 6],
[4 , 3 , 2 , 12 , 9 , 5 , 15 , 10 , 11 , 14 , 1 , 7 , 6 , 0 , 8 , 13]];

let s7 = [[4 , 11 , 2 , 14 , 15 , 0 , 8 , 13 , 3 , 12 , 9 , 7 , 5 , 10 , 6 , 1],
[13 , 0 , 11 , 7 , 4 , 9 , 1 , 10 , 14 , 3 , 5 , 12 , 2 , 15 , 8 , 6],
[1 , 4 , 11 , 13 , 12 , 3 , 7 , 14 , 10 , 15 , 6 , 8 , 0 , 5 , 9 , 2],
[6 , 11 , 13 , 8 , 1 , 4 , 10 , 7 , 9 , 5 , 0 , 15 , 14 , 2 , 3 , 12]];

let s8 = [[13 , 2 , 8 , 4 , 6 , 15 , 11 , 1 , 10 , 9 , 3 , 14 , 5 , 0 , 12 , 7],
[1 , 15 , 13 , 8 , 10 , 3 , 7 , 4 , 12 , 5 , 6 , 11 , 0 , 14 , 9 , 2],
[7 , 11 , 4 , 1 , 9 , 12 , 14 , 2 , 0 , 6 , 10 , 13 , 15 , 3 , 5 , 8],
[2 , 1 , 14 , 7 , 4 , 10 , 8 , 13 , 15 , 12 , 9 , 0 , 3 , 5 , 6 , 11]];

let p = [[16 , 7 , 20 , 21],
[29 , 12 , 28 , 17],
[1 , 15 , 23 , 26],
[5 , 18 , 31 , 10],
[2 , 8 , 24 , 14],
[32 , 27 , 3 , 9],
[19 , 13 , 30 , 6],
[22 , 11 , 4 , 25]];

export default App;
